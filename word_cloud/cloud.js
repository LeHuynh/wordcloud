class WordCloud {
  static dispatch = d3.dispatch;
  static RADIANS = Math.PI / 180;
  static SPIRALS = {
    archimedean: this.archimedeanSpiral,
    rectangular: this.rectangularSpiral,
  };
  static cw = (1 << 11) >> 5;
  static ch = 1 << 11;
  static random = Math.random;

  constructor() {
    var size = [256, 256],
      text = this.cloudText,
      font = this.cloudFont,
      fontSize = this.cloudFontSize,
      fontStyle = this.cloudFontNormal,
      fontWeight = this.cloudFontNormal,
      rotate = this.cloudRotate,
      padding = this.cloudPadding,
      spiral = this.archimedeanSpiral,
      words = [],
      timeInterval = Infinity,
      event = WordCloud.dispatch("word", "end"),
      timer = null,
      random = Math.random,
      cloud = {},
      canvas = this.cloudCanvas,
      mask = false;
    var maskBoard = null;
    this.zoom = null;
    this.words = [];
    this.currentMaskIndex = 0;
    this.masks = [];
    this.wordData = wordData;
    this.maskFiles = maskFiles;
    this.cloudInstance = null;
    this._cloud = cloud;
    const that = this;

    cloud.canvas = function (_) {
      return arguments.length ? ((canvas = functor(_)), cloud) : canvas;
    };

    cloud.data = function (data) {
      words = data;
      return cloud;
    };

    cloud.start = function () {
      var contextAndRatio = getContext(canvas()),
        board = mask ? maskBoard : that.zeroArray((size[0] >> 5) * size[1]),
        bounds = null,
        n = words.length,
        i = -1,
        tags = [],
        data = words
          .map(function (d, i) {
            d.text = text.call(this, d, i);
            d.font = font.call(this, d, i);
            d.style = fontStyle.call(this, d, i);
            d.weight = fontWeight.call(this, d, i);
            d.rotate = rotate.call(this, d, i);
            d.size = ~~fontSize.call(this, d, i);
            d.padding = padding.call(this, d, i);
            return d;
          })
          .sort((a, b) => b.size - a.size);

      if (timer) clearInterval(timer);
      timer = setInterval(step, 0);
      step();

      return cloud;

      function step() {
        var start = Date.now();
        while (Date.now() - start < timeInterval && ++i < n && timer) {
          var d = data[i];
          d.x = (size[0] * (random() + 0.5)) >> 1;
          d.y = (size[1] * (random() + 0.5)) >> 1;
          that.cloudSprite(contextAndRatio, d, data, i);
          if (d.hasText && place(board, d, bounds)) {
            tags.push(d);
            event.call("word", cloud, d);
            if (bounds) that.cloudBounds(bounds, d);
            else
              bounds = [
                { x: d.x + d.x0, y: d.y + d.y0 },
                { x: d.x + d.x1, y: d.y + d.y1 },
              ];
            // Temporary hack
            d.x -= size[0] >> 1;
            d.y -= size[1] >> 1;
          }
        }
        if (i >= n) {
          cloud.stop();
          event.call("end", cloud, tags, bounds);
        }
      }
    };

    cloud.stop = function () {
      if (timer) {
        clearInterval(timer);
        timer = null;
      }
      for (const d of words) {
        delete d.sprite;
      }
      return cloud;
    };

    function getContext(canvas) {
      const context = canvas.getContext("2d", { willReadFrequently: true });

      canvas.width = canvas.height = 1;
      const ratio = Math.sqrt(context.getImageData(0, 0, 1, 1).data.length >> 2);
      canvas.width = (WordCloud.cw << 5) / ratio;
      canvas.height = WordCloud.ch / ratio;

      context.fillStyle = context.strokeStyle = "red";

      return { context, ratio };
    }

    function place(board, tag, bounds) {
      var perimeter = [
          { x: 0, y: 0 },
          { x: size[0], y: size[1] },
        ],
        startX = tag.x,
        startY = tag.y,
        maxDelta = Math.sqrt(size[0] * size[0] + size[1] * size[1]),
        s = spiral(size),
        dt = random() < 0.5 ? 1 : -1,
        t = -dt,
        dxdy,
        dx,
        dy;

      while ((dxdy = s((t += dt)))) {
        dx = ~~dxdy[0];
        dy = ~~dxdy[1];

        if (Math.min(Math.abs(dx), Math.abs(dy)) >= maxDelta) break;

        tag.x = startX + dx;
        tag.y = startY + dy;

        if (tag.x + tag.x0 < 0 || tag.y + tag.y0 < 0 || tag.x + tag.x1 > size[0] || tag.y + tag.y1 > size[1]) continue;
        // TODO only check for collisions within current bounds.
        if (!bounds || that.collideRects(tag, bounds)) {
          if (!that.cloudCollide(tag, board, size[0])) {
            var sprite = tag.sprite,
              w = tag.width >> 5,
              sw = size[0] >> 5,
              lx = tag.x - (w << 4),
              sx = lx & 0x7f,
              msx = 32 - sx,
              h = tag.y1 - tag.y0,
              x = (tag.y + tag.y0) * sw + (lx >> 5),
              last;
            for (var j = 0; j < h; j++) {
              last = 0;
              for (var i = 0; i <= w; i++) {
                board[x + i] |= (last << msx) | (i < w ? (last = sprite[j * w + i]) >>> sx : 0);
              }
              x += sw;
            }
            return true;
          }
        }
      }
      return false;
    }

    cloud.timeInterval = function (_) {
      return arguments.length ? ((timeInterval = _ == null ? Infinity : _), cloud) : timeInterval;
    };

    cloud.mask = function (_) {
      if (arguments.length) {
        mask = _;
        maskBoard = that.createMaskBoard(_, size[0], size[1]);
        return cloud;
      } else {
        return mask;
      }
    };

    cloud.words = function (_) {
      return arguments.length ? ((words = _), cloud) : words;
    };

    cloud.size = function (_) {
      return arguments.length ? ((size = [+_[0], +_[1]]), cloud) : size;
    };

    cloud.font = function (_) {
      return arguments.length ? ((font = that.functor(_)), cloud) : font;
    };

    cloud.fontStyle = function (_) {
      return arguments.length ? ((fontStyle = that.functor(_)), cloud) : fontStyle;
    };

    cloud.fontWeight = function (_) {
      return arguments.length ? ((fontWeight = that.functor(_)), cloud) : fontWeight;
    };

    cloud.rotate = function (_) {
      return arguments.length ? ((rotate = that.functor(_)), cloud) : rotate;
    };

    cloud.text = function (_) {
      return arguments.length ? ((text = that.functor(_)), cloud) : text;
    };

    cloud.spiral = function (_) {
      return arguments.length ? ((spiral = WordCloud.SPIRALS[_] || _), cloud) : spiral;
    };

    cloud.fontSize = function (_) {
      return arguments.length ? ((fontSize = that.functor(_)), cloud) : fontSize;
    };

    cloud.padding = function (_) {
      return arguments.length ? ((padding = that.functor(_)), cloud) : padding;
    };

    cloud.random = function (_) {
      return arguments.length ? ((random = _), cloud) : random;
    };

    cloud.on = function () {
      var value = event.on.apply(event, arguments);
      return value === event ? cloud : value;
    };
  }

  initializeWordCloud() {
    if (!this.cloudInstance) {
      this.cloudInstance = new WordCloud(this.wordData, this.maskFiles);
    }
    const randomData = this.generateRandomData(500);
    this.cloudInstance.data(randomData).mask(this.cloudInstance.getCurrentMask()).start();
  }

  getRandomValue(values, deg) {
    return () => Math.floor(Math.random() * values) * deg;
  }

  generateData() {
    const priorityData = this.wordData.filter((item) => item.size > 50);
    const remainingData = this.wordData.filter((item) => item.size <= 50);
    let combinedData = [...priorityData, ...remainingData];
    return combinedData;
  }

  updateWordSizes() {
    this.wordData.forEach((word) => {
      word.size = Math.floor(Math.random() * 50) + 10;
    });
  }

  updateWordCloud() {
    this.cloudInstance.data(this.generateRandomData(500));
    this.cloudInstance.update();
  }

  handleMouseOver(d, i, nodes, node) {
    d3.select(node)
      .transition()
      .duration(200)
      .style("font-size", d.size * 1.5 + "px")
      .style("fill", "red");
  }

  handleMouseOut(d, i, nodes, node) {
    d3.select(node)
      .transition()
      .duration(200)
      .style("font-size", d.size + "px")
      .style("fill", (d) => {
        const correspondingWord = wordData.find((word) => word.text === d.text);
        return correspondingWord ? correspondingWord.color : "#000";
      });
  }

  setData(wordData) {
    this.wordData = wordData;
  }

  redraw() {
    d3.select("div#word-cloud").select("svg").select("g").attr("transform", d3.event.transform.toString());
  }

  initializeZoom() {
    if (!this.zoom) {
      this.zoom = d3.zoom().on("zoom", this.redraw.bind(this));
      d3.select("div#word-cloud").select("svg").call(this.zoom);
    }
  }

  zoomInOut() {
    const svg = d3.select("div#word-cloud").select("svg");
    svg.call(this.zoom.transform, d3.zoomIdentity.translate(-700, -700).scale(0.3));
    svg
      .transition()
      .duration(1000)
      .call(this.zoom.transform, d3.zoomIdentity.translate(400, 400).scale(0.1))
      .transition()
      .duration(1000)
      .call(this.zoom.transform, d3.zoomIdentity.translate(400, 400).scale(1.3))
      .transition()
      .duration(1000)
      .call(this.zoom.transform, d3.zoomIdentity.translate(400, 400).scale(1));
  }

  static loadMask(url) {
    return new Promise((resolve, reject) => {
      const mask = new Image();
      mask.onload = () => {
        resolve(mask);
      };
      mask.onerror = (error) => {
        reject(error);
      };
      mask.src = url;
    });
  }

  async updateWordCloud() {
    this.masks = [];
    for (const maskFile of this.maskFiles) {
      const mask = await WordCloud.loadMask(maskFile);
      this.masks.push(mask);
      this.initializeZoom();
      this.zoomInOut();
    }
  }

  updateMask() {
    this.currentMaskIndex = (this.currentMaskIndex + 1) % this.maskFiles.length;
  }

  getCurrentMask() {
    return this.masks[this.currentMaskIndex];
  }

  cloud() {
    return this._cloud;
  }

  cloudText(d) {
    return d.text;
  }

  cloudFont() {
    return "serif";
  }

  cloudFontNormal() {
    return "normal";
  }

  cloudFontSize(d) {
    return Math.sqrt(d.value);
  }

  cloudRotate() {
    return (~~(WordCloud.random() * 6) - 3) * 30;
  }

  cloudPadding() {
    return 1;
  }

  cloudSprite(contextAndRatio, d, data, di) {
    if (d.sprite) return;
    var c = contextAndRatio.context,
      ratio = contextAndRatio.ratio;
    c.clearRect(0, 0, (WordCloud.cw << 5) / ratio, WordCloud.ch / ratio);
    var x = 0,
      y = 0,
      maxh = 0,
      n = data.length;
    --di;
    while (++di < n) {
      d = data[di];
      c.save();
      c.font = d.style + " " + d.weight + " " + ~~((d.size + 1) / ratio) + "px " + d.font;
      const metrics = c.measureText(d.text);
      const anchor = -Math.floor(metrics.width / 2);
      let w = (metrics.width + 1) * ratio;
      let h = d.size << 1;
      if (d.rotate) {
        var sr = Math.sin(d.rotate * WordCloud.RADIANS),
          cr = Math.cos(d.rotate * WordCloud.RADIANS),
          wcr = w * cr,
          wsr = w * sr,
          hcr = h * cr,
          hsr = h * sr;
        w = ((Math.max(Math.abs(wcr + hsr), Math.abs(wcr - hsr)) + 0x1f) >> 5) << 5;
        h = ~~Math.max(Math.abs(wsr + hcr), Math.abs(wsr - hcr));
      } else {
        w = ((w + 0x1f) >> 5) << 5;
      }
      if (h > maxh) maxh = h;
      if (x + w >= WordCloud.cw << 5) {
        x = 0;
        y += maxh;
        maxh = 0;
      }
      if (y + h >= WordCloud.ch) break;
      c.translate((x + (w >> 1)) / ratio, (y + (h >> 1)) / ratio);
      if (d.rotate) c.rotate(d.rotate * WordCloud.RADIANS);
      c.fillText(d.text, anchor, 0);
      if (d.padding) (c.lineWidth = 2 * d.padding), c.strokeText(d.text, anchor, 0);
      c.restore();
      d.width = w;
      d.height = h;
      d.xoff = x;
      d.yoff = y;
      d.x1 = w >> 1;
      d.y1 = h >> 1;
      d.x0 = -d.x1;
      d.y0 = -d.y1;
      d.hasText = true;
      x += w;
    }
    var pixels = c.getImageData(0, 0, (WordCloud.cw << 5) / ratio, WordCloud.ch / ratio).data,
      sprite = [];
    while (--di >= 0) {
      d = data[di];
      if (!d.hasText) continue;
      var w = d.width,
        w32 = w >> 5,
        h = d.y1 - d.y0;
      for (var i = 0; i < h * w32; i++) sprite[i] = 0;
      x = d.xoff;
      if (x == null) return;
      y = d.yoff;
      var seen = 0,
        seenRow = -1;
      for (var j = 0; j < h; j++) {
        for (var i = 0; i < w; i++) {
          var k = w32 * j + (i >> 5),
            m = pixels[((y + j) * (WordCloud.cw << 5) + (x + i)) << 2] ? 1 << (31 - (i % 32)) : 0;
          sprite[k] |= m;
          seen |= m;
        }
        if (seen) seenRow = j;
        else {
          d.y0++;
          h--;
          j--;
          y++;
        }
      }
      d.y1 = d.y0 + seenRow;
      d.sprite = sprite.slice(0, (d.y1 - d.y0) * w32);
    }
  }

  cloudCollide(tag, board, sw) {
    sw >>= 5;
    var sprite = tag.sprite,
      w = tag.width >> 5,
      lx = tag.x - (w << 4),
      sx = lx & 0x7f,
      msx = 32 - sx,
      h = tag.y1 - tag.y0,
      x = (tag.y + tag.y0) * sw + (lx >> 5),
      last;
    for (var j = 0; j < h; j++) {
      last = 0;
      for (var i = 0; i <= w; i++) {
        if (((last << msx) | (i < w ? (last = sprite[j * w + i]) >>> sx : 0)) & board[x + i]) return true;
      }
      x += sw;
    }
    return false;
  }

  cloudBounds(bounds, d) {
    var b0 = bounds[0],
      b1 = bounds[1];
    if (d.x + d.x0 < b0.x) b0.x = d.x + d.x0;
    if (d.y + d.y0 < b0.y) b0.y = d.y + d.y0;
    if (d.x + d.x1 > b1.x) b1.x = d.x + d.x1;
    if (d.y + d.y1 > b1.y) b1.y = d.y + d.y1;
  }

  collideRects(a, b) {
    return a.x + a.x1 > b[0].x && a.x + a.x0 < b[1].x && a.y + a.y1 > b[0].y && a.y + a.y0 < b[1].y;
  }

  archimedeanSpiral(size) {
    var e = size[0] / size[1];
    return function (t) {
      return [e * (t *= 0.1) * Math.cos(t), t * Math.sin(t)];
    };
  }

  rectangularSpiral(size) {
    var dy = 4,
      dx = (dy * size[0]) / size[1],
      x = 0,
      y = 0;
    return function (t) {
      var sign = t < 0 ? -1 : 1;
      switch ((Math.sqrt(1 + 4 * sign * t) - sign) & 3) {
        case 0:
          x += dx;
          break;
        case 1:
          y += dy;
          break;
        case 2:
          x -= dx;
          break;
        default:
          y -= dy;
          break;
      }
      return [x, y];
    };
  }

  zeroArray(n) {
    var a = [],
      i = -1;
    while (++i < n) a[i] = 0;
    return a;
  }

  cloudCanvas() {
    return document.createElement("canvas");
  }

  functor(d) {
    return typeof d === "function"
      ? d
      : function () {
          return d;
        };
  }

  get_mask_pixels(mask, w, h) {
    var canvas = document.createElement("canvas");
    canvas.width = w;
    canvas.height = h;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(mask, 0, 0, w, h);
    return ctx.getImageData(0, 0, w, h).data;
  }

  create_mask_board_from_pixels(mask_pixels, w, h) {
    var w32 = w >> 5;
    var sprite = [];
    for (var i = 0; i < h * w32; i++) {
      sprite[i] = 0;
    }
    for (var j = 0; j < h; j++) {
      for (var i = 0; i < w; i++) {
        var k = w32 * j + (i >> 5);
        var m = mask_pixels[(j * w + i) << 2] ? 1 << (31 - (i % 32)) : 0;
        sprite[k] |= m;
      }
    }
    return sprite.slice(0, w * w32);
  }

  createMaskBoard(mask, w, h) {
    var pixels = this.get_mask_pixels(mask, w, h);
    var board = this.create_mask_board_from_pixels(pixels, w, h);
    return board;
  }
}
